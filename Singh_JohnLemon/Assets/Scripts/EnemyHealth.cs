﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health = 3; //set enemy health in editor

    public void TakeDamage(int damageAmount) //called to damage the enemy
    {
        health -= damageAmount; //subtract from total current health

        if (health <= 0) //if health is 0
        {
            Destroy(gameObject); //kill the enemy
        }
    }
}
