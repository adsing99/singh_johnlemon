﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    public Renderer renderer; //create variables for material swap for stealth
    public Material matJohnPBR;
    public Material matJohnToon;
    public Material matJohnTransparent;

    public GameObject projectilePrefab; //prefab reference to projectice asset being cloned
    public Transform shotSpawn; //drag and drop reference to empty game object we added and positioned for where the bullets will spawn
    public float shotSpeed = 10f; //bullet speed
    public int ammo = 15; // ammmo count

    public bool invulnerable; //whether stealthed or not
    float invulnerableCooldown; //how long stealth lasts
    public int charge = 3;  //how many times you can stealth

    public TextMeshProUGUI shots; //UI 
    public TextMeshProUGUI stealths; //UI 

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        SetText();
    }

    private void Update()
    {
        if (invulnerable)
        {
            invulnerableCooldown -= Time.deltaTime; //start stealth timer
            if (invulnerableCooldown <= 0f) //end stealth when timers at 0
            {
                invulnerable = false;
                Material[] mats = new Material[] { matJohnToon, matJohnPBR }; //return to normal
                renderer.materials = mats;
                charge -= 1;
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && invulnerableCooldown <= 0f && charge > 0) //if key is pressed and not already on start stealth
        {
            invulnerable = true;
            invulnerableCooldown = 3f; //lasts 3 secs
            Material[] mats = new Material[] { matJohnTransparent, matJohnTransparent }; //turn transparent 
            renderer.materials = mats;
        }


        if (Input.GetKeyDown(KeyCode.Space) && (ammo > 0)) //shoot when space is pressed
        {
            GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation); //give projectiles rigidbody velocity flying forward based on forward vector

            Rigidbody projectileRB = projectile.GetComponent<Rigidbody>();
            projectileRB.velocity = transform.forward * shotSpeed;
            ammo -= 1;
        }

        SetText();
    }

    void SetText()
    {
        shots.text = "Ammo: " + ammo.ToString();
        stealths.text = "Stealth: " + charge.ToString();
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}