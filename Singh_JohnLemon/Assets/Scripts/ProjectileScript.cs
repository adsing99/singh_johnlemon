﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ProjectileScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    { //when bullet collides
        if (!other.isTrigger) //ignore trigger colliders
        {
            if (other.gameObject.CompareTag("Enemy")) //if an enemy is hit
            {
                EnemyHealth eHealth = other.gameObject.GetComponent<EnemyHealth>(); //reference EnemyHealth script on the enemy hit

                if (eHealth != null)
                    eHealth.TakeDamage(1); //do 1 damage to the enemy
            }

            Destroy(gameObject); //destroy the object
        }
    }

}
